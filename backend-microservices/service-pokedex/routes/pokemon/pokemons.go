package pokemon

import (
	"encoding/json"
	"fmt"
	"log"
	"pokedex/db"
	"strings"

	"github.com/gofiber/fiber/v2"
)

func GetPokemonHint(c *fiber.Ctx) error {
	queries := c.Queries()
	search := queries["name"]

	query := map[string]interface{}{
		"query": map[string]interface{}{
			"bool": map[string]interface{}{
				"must": []map[string]interface{}{
					{
						"wildcard": map[string]interface{}{
							"name": "*" + search + "*",
						},
					},
				},
			},
		},
	}

	res, err := db.ES.Search(
		db.ES.Search.WithIndex("pokemons"),
		db.ES.Search.WithBody(buildQuery(query)),
	)

	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()

	var result map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&result); err != nil {
		log.Fatal(err)
	}

	indexes, err := parseIndexes(result)
	if err != nil {
		log.Fatal(err)
	}

	c.JSON(indexes)
	return c.SendStatus(fiber.StatusOK)
}

func buildQuery(query map[string]interface{}) *strings.Reader {
	body, err := json.Marshal(query)
	if err != nil {
		log.Fatal(err)
	}
	return strings.NewReader(string(body))
}

func parseIndexes(result map[string]interface{}) ([]interface{}, error) {
	var indexes []interface{}

	hits, ok := result["hits"].(map[string]interface{})["hits"].([]interface{})
	if !ok {
		return nil, fmt.Errorf("failed to parse hits from Elasticsearch response")
	}

	for _, hit := range hits {
		index, ok := hit.(map[string]interface{})["_source"]
		if !ok {
			return nil, fmt.Errorf("failed to parse index from Elasticsearch response")
		}
		indexes = append(indexes, index)
	}

	return indexes, nil
}
