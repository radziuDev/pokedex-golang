package pokemon

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"log"
	"pokedex/db"
)

type Pokemon struct {
	ID                     int             `json:"id"`
	Name                   string          `json:"name"`
	Abilities              json.RawMessage `json:"abilities"`
	BaseExperience         int             `json:"base_experience"`
	Forms                  json.RawMessage `json:"forms"`
	GameIndices            json.RawMessage `json:"game_indices"`
	Height                 int             `json:"height"`
	HeldItems              json.RawMessage `json:"held_items"`
	IsDefault              bool            `json:"is_default"`
	LocationAreaEncounters string          `json:"location_area_encounters"`
	Moves                  json.RawMessage `json:"moves"`
	Order                  int             `json:"order"`
	PastTypes              json.RawMessage `json:"past_types"`
	Species                json.RawMessage `json:"species"`
	Sprites                json.RawMessage `json:"sprites"`
	Stats                  json.RawMessage `json:"stats"`
	Types                  json.RawMessage `json:"types"`
	Weight                 int             `json:"weight"`
}

func GetPokemon(c *fiber.Ctx) error {
	queries := c.Queries()
	name := queries["name"]

	row := db.DB.QueryRow("SELECT * FROM pokemons WHERE name = $1", name)

	var pokemon Pokemon
	var abilities, forms, gameIndices, heldItems, moves, pastTypes, species, sprites, stats, types []uint8

	err := row.Scan(
		&pokemon.ID,
		&pokemon.Name,
		&abilities,
		&pokemon.BaseExperience,
		&forms,
		&gameIndices,
		&pokemon.Height,
		&heldItems,
		&pokemon.IsDefault,
		&pokemon.LocationAreaEncounters,
		&moves,
		&pokemon.Order,
		&pastTypes,
		&species,
		&sprites,
		&stats,
		&types,
		&pokemon.Weight,
	)

	if err != nil {
		log.Println(err)
		return c.SendStatus(fiber.StatusNotFound)
	}

	pokemon.Abilities = json.RawMessage(abilities)
	pokemon.Forms = json.RawMessage(forms)
	pokemon.GameIndices = json.RawMessage(gameIndices)
	pokemon.HeldItems = json.RawMessage(heldItems)
	pokemon.Moves = json.RawMessage(moves)
	pokemon.PastTypes = json.RawMessage(pastTypes)
	pokemon.Species = json.RawMessage(species)
	pokemon.Sprites = json.RawMessage(sprites)
	pokemon.Stats = json.RawMessage(stats)
	pokemon.Types = json.RawMessage(types)

	jsonData, err := json.Marshal(pokemon)
	if err != nil {
		log.Println(err)
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	c.Send(jsonData)
	return c.SendStatus(fiber.StatusOK)
}
