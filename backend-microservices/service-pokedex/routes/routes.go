package routes

import (
	"fmt"
	"log"
	"pokedex/config"
	"pokedex/middlewere"
	"pokedex/routes/pokemon"

	"github.com/gofiber/fiber/v2"
)

func Init() {
	app := fiber.New()

	app.Get("/pokedex/pokemons", middlewere.AuthMiddlewere, pokemon.GetPokemonHint)
	app.Get("/pokedex/pokemon", middlewere.AuthMiddlewere, pokemon.GetPokemon)

	log.Fatalln(app.Listen(fmt.Sprintf(":%v", config.Config.Port)))
	log.Printf("User service runnig at port %v", config.Config.Port)
}
