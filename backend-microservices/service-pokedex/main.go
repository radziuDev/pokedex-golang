package main

import (
	"log"
	"pokedex/config"
	"pokedex/db"
	"pokedex/routes"
	"pokedex/tasks"

	_ "github.com/lib/pq"
)

func main() {
  config.LoadConfigs()
  err := db.InitPostgres()

	if err != nil {
		log.Println(err)
		return
  }

  res, err := db.InitElasticSearch()
	if err != nil {
		log.Println(err)
		return
  }
	defer res.Body.Close()

  err = tasks.InitPokedex()
	if err != nil {
		log.Println(err)
		return
  }

  routes.Init()
}
