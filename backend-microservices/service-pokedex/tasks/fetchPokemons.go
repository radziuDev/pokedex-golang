package tasks

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"pokedex/db"
	"pokedex/utils"
	"strconv"

	"github.com/elastic/go-elasticsearch/v7"
	"github.com/lib/pq"
)

type Pokemon struct {
	ID                     int                      `json:"id"`
	Name                   string                   `json:"name"`
	Abilities              []json.RawMessage        `json:"abilities"`
	BaseExperience         int                      `json:"base_experience"`
	Forms                  []map[string]string      `json:"forms"`
	GameIndices            []map[string]interface{} `json:"game_indices"`
	Height                 int                      `json:"height"`
	HeldItems              []interface{}            `json:"held_items"`
	IsDefault              bool                     `json:"is_default"`
	LocationAreaEncounters string                   `json:"location_area_encounters"`
	Moves                  []map[string]interface{} `json:"moves"`
	Order                  int                      `json:"order"`
	PastTypes              []interface{}            `json:"past_types"`
	Species                map[string]string        `json:"species"`
	Sprites                map[string]interface{}   `json:"sprites"`
	Stats                  []map[string]interface{} `json:"stats"`
	Types                  []map[string]interface{} `json:"types"`
	Weight                 int                      `json:"weight"`
}

func InitPokedex() error {
	var returnError error = nil

	isEmpty, err := isTableEmpty(db.DB)
	if err != nil {
		returnError = err
		return returnError
	} else if !isEmpty {
		return nil
	}

	var pokemons []Pokemon
	index := 1

	for true {
		body, err := fetchPokemonFromAPI(strconv.Itoa(index))

		if err != nil {
			returnError = err
			break
		}

		if string(body) == "Not Found" {
			utils.PrintProgressBar(1000, 1000, "Progress", "Complete", 25, "=")
			bulkErr := bulkInsertData(db.DB, pokemons)

			if bulkErr != nil {
				returnError = bulkErr
				break
			}

			pokemons, fetchErr := fetchPokemonsFromDB(db.DB)
			if err != nil {
				returnError = fetchErr
				break
			}

			indexErr := indexPokemons(db.ES, pokemons)
			if err != nil {
				returnError = indexErr
				break
			}
			break
		}

		var data Pokemon
		jsonErr := json.Unmarshal([]byte(body), &data)

		if jsonErr != nil {
			returnError = jsonErr
			break
		}

		pokemons = append(pokemons, data)
		index += 1

		if index < 1000 {
			utils.PrintProgressBar(index, 1000, "Progress", "Complete", 25, "=")
		}
	}

	return returnError
}

func fetchPokemonFromAPI(value string) ([]byte, error) {
	url := fmt.Sprintf("https://pokeapi.co/api/v2/pokemon/%s", value)
	resp, err := http.Get(url)

	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	return body, nil
}

func isTableEmpty(db *sql.DB) (bool, error) {
	var count int
	err := db.QueryRow("SELECT COUNT(*) FROM pokemons").Scan(&count)
	if err != nil {
		return false, err
	}
	return count == 0, nil
}

func bulkInsertData(db *sql.DB, data []Pokemon) error {
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	stmt, err := tx.Prepare(pq.CopyIn("pokemons",
		"base_experience",
		"height",
		"is_default",
		"location_area_encounters",
		"name",
		"order",
		"species",
		"weight",
		"abilities",
		"forms",
		"game_indices",
		"held_items",
		"moves",
		"past_types",
		"sprites",
		"stats",
		"types",
	))
	if err != nil {
		return err
	}
	defer stmt.Close()

	for _, d := range data {
		species, _ := json.Marshal(d.Species)
		abilitiesJSON, _ := json.Marshal(d.Abilities)
		formsJSON, _ := json.Marshal(d.Forms)
		gameIndicesJSON, _ := json.Marshal(d.GameIndices)
		heldItemsJSON, _ := json.Marshal(d.HeldItems)
		movesJSON, _ := json.Marshal(d.Moves)
		pastTypesJSON, _ := json.Marshal(d.PastTypes)
		spritesJSON, _ := json.Marshal(d.Sprites)
		statsJSON, _ := json.Marshal(d.Stats)
		typesJSON, _ := json.Marshal(d.Types)

		_, err = stmt.Exec(
			d.BaseExperience,
			d.Height,
			d.IsDefault,
			d.LocationAreaEncounters,
			d.Name,
			d.Order,
			string(species),
			d.Weight,
			string(abilitiesJSON),
			string(formsJSON),
			string(gameIndicesJSON),
			string(heldItemsJSON),
			string(movesJSON),
			string(pastTypesJSON),
			string(spritesJSON),
			string(statsJSON),
			string(typesJSON),
		)
		if err != nil {
			return err
		}
	}

	_, err = stmt.Exec()
	if err != nil {
		return err
	}

	err = stmt.Close()
	if err != nil {
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

type PokemonIndex struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

func fetchPokemonsFromDB(db *sql.DB) ([]PokemonIndex, error) {
	rows, err := db.Query("SELECT id, name FROM pokemons")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var pokemons []PokemonIndex

	for rows.Next() {
		var pokemon PokemonIndex
		err := rows.Scan(&pokemon.Id, &pokemon.Name)
		if err != nil {
			return nil, err
		}
		pokemons = append(pokemons, pokemon)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return pokemons, nil
}

func indexPokemons(es *elasticsearch.Client, pokemons []PokemonIndex) error {
	for _, pokemon := range pokemons {
		body, err := json.Marshal(pokemon)
		if err != nil {
			return err
		}

		_, err = es.Index("pokemons", bytes.NewReader(body))
		if err != nil {
			return err
		}
	}

	return nil
}
