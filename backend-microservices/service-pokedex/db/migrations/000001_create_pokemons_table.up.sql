CREATE TABLE pokemons (
    id SERIAL PRIMARY KEY,
    name TEXT,
    abilities JSONB,
    base_experience INT,
    forms JSONB,
    game_indices JSONB,
    height INT,
    held_items JSONB,
    is_default BOOLEAN,
    location_area_encounters TEXT,
    moves JSONB,
    "order" INT,
    past_types JSONB,
    species JSONB,
    sprites JSONB,
    stats JSONB,
    types JSONB,
    weight INT
);
