package db

import (
	"database/sql"
	"fmt"
	"pokedex/config"
)

var DB *sql.DB

func InitPostgres() error {
	connStr := fmt.Sprintf("postgres://postgres:postgres@service-pokedex-db:%v/pokedex?sslmode=disable", config.Config.PostgresPort)

	db, err := sql.Open("postgres", connStr)
	DB = db

	return err
}
