package db

import (
	"fmt"
	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"
	"pokedex/config"
)

var ES *elasticsearch.Client

func InitElasticSearch() (*esapi.Response, error) {
	address := fmt.Sprintf("http://service-pokedex-elasticsearch:%v", config.Config.ElasticSearchPort)
	cfg := elasticsearch.Config{
		Addresses: []string{address},
	}

	es, err := elasticsearch.NewClient(cfg)
	if err != nil {
		return nil, err
	}

	res, err := es.Info()
	if err != nil {
		return nil, err
	}

	ES = es
	return res, nil
}
