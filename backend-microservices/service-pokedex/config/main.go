package config

type ConfigStruct struct {
	Port              int
	UserServicePort   int
	ElasticSearchPort int
	PostgresPort      int
}

var Config ConfigStruct

func LoadConfigs() {
	setConfig("Port", "PORT", true, false, "3000")
	setConfig("UserServicePort", "USER_SERVICE_PORT", true, true, "")
	setConfig("ElasticSearchPort", "ELASTICSEARCH_PORT", true, true, "")
	setConfig("PostgresPort", "POSTGRES_PORT", true, true, "")
}
