package infrastructure

import (
	"errors"

	"github.com/neo4j/neo4j-go-driver/v5/neo4j"
)

func DeleteFriendRequest(fromId int64, toId int64) error {
	_, err := GetUser(fromId)
	if err != nil {
		return err
	}
	_, err = GetUser(toId)
	if err != nil {
		return err
	}

	_, err = neo4j.ExecuteQuery(Ctx, Driver,
		"MATCH (u1:User {id: $id1})-[req:FRIEND_REQUEST]->(u2:User {id:$id2}) DELETE req",
		map[string]any{
			"id1": fromId,
			"id2": toId,
		}, neo4j.EagerResultTransformer)

	_, err = neo4j.ExecuteQuery(Ctx, Driver,
		"MATCH (u1:User {id: $id1})<-[req:FRIEND_REQUEST]-(u2:User {id:$id2}) DELETE req",
		map[string]any{
			"id1": fromId,
			"id2": toId,
		}, neo4j.EagerResultTransformer)

	return err
}

func CreateFriendRequest(fromId int64, toId int64) error {
	_, err := GetUser(fromId)
	if err != nil {
		return err
	}
	_, err = GetUser(toId)
	if err != nil {
		return err
	}

	result, err := neo4j.ExecuteQuery(Ctx, Driver,
		"MATCH (u:User {id: $id1})-[:FRIEND_REQUEST]->(request:User {id:$id2}) return request.id, request.login",
		map[string]any{
			"id1": fromId,
			"id2": toId,
		}, neo4j.EagerResultTransformer)

	if err != nil {
		return err
	}

	if len(result.Records) > 0 {
		return errors.New("Request have been sent")
	}

	_, err = neo4j.ExecuteQuery(Ctx, Driver,
		"MATCH (u1:User {id: $id1}), (u2:User {id: $id2}) CREATE (u1)-[:FRIEND_REQUEST]->(u2)",
		map[string]any{
			"id1": fromId,
			"id2": toId,
		}, neo4j.EagerResultTransformer)

	return err
}

func GetFriendsRequest(userId int64) ([]User, error) {
	var users []User = []User{}
	_, err := GetUser(userId)
	if err != nil {
		return users, err
	}

	result, err := neo4j.ExecuteQuery(Ctx, Driver,
		"MATCH (u:User {id: $id})<-[:FRIEND_REQUEST]-(request:User) return request.id, request.login",
		map[string]any{
			"id": userId,
		}, neo4j.EagerResultTransformer)

	for _, record := range result.Records {
		user := User{
			ID:    record.Values[0].(int64),
			Login: record.Values[1].(string),
		}
		users = append(users, user)
	}

	return users, err
}

func InsertUser(user User) error {
	_, err := GetUser(user.ID)
	if err == nil || err.Error() != "User not exist" {
		return err
	}

	_, err = neo4j.ExecuteQuery(Ctx, Driver,
		"CREATE (u:User {id: $id, login: $login})",
		map[string]any{
			"id":    user.ID,
			"login": user.Login,
		}, neo4j.EagerResultTransformer)

	return err
}

func GetUsersByMatch(value string) ([]User, error) {
	var users []User = []User{}

	result, err := neo4j.ExecuteQuery(Ctx, Driver,
		"MATCH (u:User) WHERE u.login CONTAINS $value RETURN u.id, u.login",
		map[string]interface{}{
			"value": value,
		}, neo4j.EagerResultTransformer)

	if err != nil {
		return users, err
	}

	for _, record := range result.Records {
		user := User{
			ID:    record.Values[0].(int64),
			Login: record.Values[1].(string),
		}
		users = append(users, user)
	}

	return users, nil
}

func GetUser(id int64) (User, error) {
	result, err := neo4j.ExecuteQuery(Ctx, Driver,
		"MATCH (u:User {id: $id}) RETURN u.id, u.login",
		map[string]interface{}{
			"id": id,
		}, neo4j.EagerResultTransformer)
	if err != nil {
		return User{}, err
	}

	if len(result.Records) == 0 {
		return User{}, errors.New("User not exist")
	}

	userRecord := result.Records[0]
	user := User{
		ID:    userRecord.Values[0].(int64),
		Login: userRecord.Values[1].(string),
	}

	return user, err
}
