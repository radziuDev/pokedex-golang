package infrastructure

import (
	"errors"

	"github.com/neo4j/neo4j-go-driver/v5/neo4j"
)

func GetUsersForRoom(roomId string) ([]User, error) {
	var users []User = []User{}
	_, err := GetRoom(roomId)
	if err != nil {
		return users, err
	}

	result, err := neo4j.ExecuteQuery(Ctx, Driver,
		"MATCH (r:Room {id: $id})<-[:JOINED]-(u:User) RETURN u.id, u.login",
		map[string]interface{}{
			"id": roomId,
		}, neo4j.EagerResultTransformer)
	if err != nil {
		return users, err
	}

	for _, record := range result.Records {
		user := User{
			ID:    record.Values[0].(int64),
			Login: record.Values[1].(string),
		}
		users = append(users, user)
	}

	return users, err
}

func GetRoomsForUser(userId int64) ([]Room, error) {
	var rooms []Room = []Room{}
	result, err := neo4j.ExecuteQuery(Ctx, Driver,
		"MATCH (u:User {id: $id})-[:JOINED]->(room:Room) RETURN room.id",
		map[string]interface{}{
			"id": userId,
		}, neo4j.EagerResultTransformer)

	if err != nil {
		return rooms, err
	}

	for _, record := range result.Records {
		room := Room{
			ID: record.Values[0].(string),
		}
		rooms = append(rooms, room)
	}

	return rooms, nil
}

func JoinRoom(id int64, roomId string) error {
	_, err := GetUser(id)
	if err != nil {
		return err
	}

	_, err = GetRoom(roomId)
	if err != nil {
		if err.Error() != "Room not exist" {
			return err
		}
		err = CreateRoom(roomId)
		if err != nil {
			return err
		}
	}

	result, err := neo4j.ExecuteQuery(Ctx, Driver,
		"MATCH (u:User {id: $id})-[r:JOINED]->(room:Room {id: $roomId}) RETURN COUNT(r) > 0  AS connectionExist",
		map[string]any{
			"id":     id,
			"roomId": roomId,
		}, neo4j.EagerResultTransformer)

	if len(result.Records) > 0 {
		connectionExist := result.Records[0].Values[0].(bool)
		if connectionExist {
			return err
		}
	}

	_, err = neo4j.ExecuteQuery(Ctx, Driver,
		"MATCH (u:User {id: $id}), (r:Room {id: $roomId}) CREATE (u)-[:JOINED]->(r)",
		map[string]any{
			"id":     id,
			"roomId": roomId,
		}, neo4j.EagerResultTransformer)

	return err
}

func CreateRoom(roomName string) error {
	_, err := neo4j.ExecuteQuery(Ctx, Driver,
		"CREATE (r:Room {id: $id})",
		map[string]any{
			"id": roomName,
		}, neo4j.EagerResultTransformer)

	return err
}

func GetRoom(roomId string) (Room, error) {
	result, err := neo4j.ExecuteQuery(Ctx, Driver,
		"MATCH (r:Room {id: $id}) RETURN r.id",
		map[string]interface{}{
			"id": roomId,
		}, neo4j.EagerResultTransformer)
	if err != nil {
		return Room{}, err
	}

	if len(result.Records) == 0 {
		return Room{}, errors.New("Room not exist")
	}

	roomRecord := result.Records[0]
	room := Room{
		ID: roomRecord.Values[0].(string),
	}

	return room, nil
}
