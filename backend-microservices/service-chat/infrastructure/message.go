package infrastructure

import "github.com/neo4j/neo4j-go-driver/v5/neo4j"

func GetMessagesFromRoom(roomId string) ([]RoomMessage, error) {
	var messages []RoomMessage = []RoomMessage{}

	result, err := neo4j.ExecuteQuery(Ctx, Driver,
		"MATCH (room:Room {id: $id})<-[:SENT]-(message:Message) OPTIONAL MATCH (message)<-[:CREATED]-(user:User) RETURN message.text, message.date, user.login",
		map[string]any{
			"id": roomId,
		}, neo4j.EagerResultTransformer)
	if err != nil {
		return messages, err
	}

	for _, record := range result.Records {
		roomMessage := RoomMessage{
			Text: record.Values[0].(string),
			Date: record.Values[1].(string),
			User: record.Values[2].(string),
		}
		messages = append(messages, roomMessage)
	}

	return messages, nil
}

func SendMessage(userId int64, roomId string, message Message) error {
	_, err := GetUser(userId)
	if err != nil {
		return err
	}

	_, err = GetRoom(roomId)
	if err != nil {
		return err
	}

	_, err = neo4j.ExecuteQuery(Ctx, Driver,
		"CREATE (m:Message {id: $messageId, text: $text, date: $date})",
		map[string]any{
			"messageId": message.ID,
			"text":      message.Text,
			"date":      message.Date,
		}, neo4j.EagerResultTransformer)
	if err != nil {
		return err
	}

	_, err = neo4j.ExecuteQuery(Ctx, Driver,
		"MATCH (u:User {id: $userId}), (m:Message {id: $messageId}) CREATE (u)-[:CREATED]->(m)",
		map[string]any{
			"messageId": message.ID,
			"userId":    userId,
		}, neo4j.EagerResultTransformer)
	if err != nil {
		return err
	}

	_, err = neo4j.ExecuteQuery(Ctx, Driver,
		"MATCH (r:Room {id: $roomId}), (m:Message {id: $messageId}) CREATE (m)-[:SENT]->(r)",
		map[string]any{
			"messageId": message.ID,
			"roomId":    roomId,
		}, neo4j.EagerResultTransformer)

	return err
}
