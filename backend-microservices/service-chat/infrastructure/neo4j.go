package infrastructure

import (
	"chat/config"
	"context"

	"github.com/neo4j/neo4j-go-driver/v5/neo4j"
)

var Ctx context.Context
var Driver neo4j.DriverWithContext

func InitNeo4j() {
	driver, err := neo4j.NewDriverWithContext(
		config.Config.Neo4jUrl,
		neo4j.BasicAuth(config.Config.Neo4jUser, config.Config.Neo4jPassword, ""),
	)
	if err != nil {
		panic(err)
	}

	Ctx = context.Background()
	Driver = driver
}
