package infrastructure

type User struct {
	ID    int64  `json:"id"`
	Login string `json:"login"`
}

type Room struct {
	ID string `json:"id"`
}

type Message struct {
	ID   string `json:"id"`
	Text string `json:"text"`
	Date string `json:"date"`
}

type RoomMessage struct {
	Text string `json:"text"`
	Date string `json:"date"`
	User string `json:"user"`
}
