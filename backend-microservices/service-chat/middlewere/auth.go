package middlewere

import (
	"chat/config"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

type User struct {
	Id    int    `json:"Id"`
	Login string `json:"Login"`
}

func AuthMiddlewere(c *fiber.Ctx) error {
	headers := c.GetReqHeaders()
	auth := headers["Authorization"]

  user, err := GetUser(auth)
  if err != nil {
    if err == errors.New("Unauthorized") {
      return fiber.NewError(fiber.StatusUnauthorized, "Unauthorized")
    }
		return fiber.NewError(fiber.StatusInternalServerError, "Internal error")
  }

	c.Locals("user", user)
	return c.Next()
}

func GetUser (auth string) (User, error) {
	if auth == "" {
		return User{}, errors.New("Unauthorized")
	}

	req, err := http.NewRequest("GET", fmt.Sprintf("%v/user/me", config.Config.UserServiceUrl), nil)
	if err != nil {
		return User{}, errors.New("Internal Error")
	}

	req.Header.Add("Authorization", auth)
	client := http.DefaultClient
	resp, err := client.Do(req)
	if err != nil {
		return User{}, errors.New("Internal Error")
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusUnauthorized {
		return User{}, errors.New("Unauthorized")
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return User{}, errors.New("Internal Error")
	}

	var user User
	err = json.Unmarshal(body, &user)
	if err != nil {
		return User{}, errors.New("Internal Error")
	}

  return user, nil
}
