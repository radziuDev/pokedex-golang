package config

type ConfigStruct struct {
	Port           int
	UserServiceUrl string
	RabbitMqUrl    string
	Neo4jUrl       string
	Neo4jUser      string
	Neo4jPassword  string
}

var Config ConfigStruct

func LoadConfigs() {
	setConfig("Port", "PORT", true, false, "3000")
	setConfig("UserServiceUrl", "USER_SERVICE_URL", false, true, "")
	setConfig("RabbitMqUrl", "RABBITMQ_URL", false, true, "")
	setConfig("Neo4jUrl", "NEO4J_URL", false, true, "")
	setConfig("Neo4jUser", "NEO4J_USER", false, true, "")
	setConfig("Neo4jPassword", "NEO4J_PASSWORD", false, true, "")
}
