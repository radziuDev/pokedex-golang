package main

import (
	"chat/config"
	"chat/infrastructure"
	"chat/listener"
	"chat/routes"
	"sync"
)

func main() {
	config.LoadConfigs()

	infrastructure.InitNeo4j()
	defer infrastructure.Driver.Close(infrastructure.Ctx)

  var wg sync.WaitGroup
  wg.Add(2)

	go listener.InitRabbitMq(&wg)
  go routes.InitAPI(&wg)
  
	wg.Wait()
}
