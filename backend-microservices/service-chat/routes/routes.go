package routes

import (
	"chat/config"
	"chat/middlewere"
	"fmt"
	"log"
	"sync"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/websocket/v2"
)

func InitAPI(wg *sync.WaitGroup) {
	defer wg.Done()

	app := fiber.New()
	app.Get("/chat/friends", middlewere.AuthMiddlewere, GetUsers)
	app.Post("/chat/friends", middlewere.AuthMiddlewere, addFriend)
	app.Post("/chat/friends/request", middlewere.AuthMiddlewere, addFriendRequest)
	app.Get("/chat/friends/request", middlewere.AuthMiddlewere, getFriendsRequests)
	app.Get("/chat/friends/rooms", middlewere.AuthMiddlewere, getRoomsForUser)
	app.Get("/chat/messages/:roomID", middlewere.AuthMiddlewere, getMessagesForRoom)
	app.Get("/chat/:roomID", websocket.New(handleWebSocketChat))

	log.Fatalln(app.Listen(fmt.Sprintf(":%v", config.Config.Port)))
	log.Printf("User service runnig at port %v", config.Config.Port)
}
