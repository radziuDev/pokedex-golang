package routes

import (
	"chat/infrastructure"
	"chat/middlewere"
	"encoding/json"
	"log"
	"net/url"
	"sync"
	"time"

	"github.com/gofiber/websocket/v2"
	"github.com/google/uuid"
)

type Client struct {
	conn   *websocket.Conn
	roomID string
}

var rooms = struct {
	sync.RWMutex
	clients map[*Client]struct{}
}{clients: make(map[*Client]struct{})}

type Message struct {
	Authorization string `json:"authorization"`
	Message       string `json:"message"`
}

type Response struct {
	Name    string `json:"name"`
	Message string `json:"message"`
}

func handleWebSocketChat(c *websocket.Conn) {
	encodedRoomID := c.Params("roomID")
	roomID, err := url.QueryUnescape(encodedRoomID)
	if err != nil {
		return
	}

	client := &Client{conn: c, roomID: roomID}

	rooms.Lock()
	rooms.clients[client] = struct{}{}
	rooms.Unlock()
	defer func() {
		rooms.Lock()
		delete(rooms.clients, client)
		rooms.Unlock()
		c.Close()
	}()

	for {
		_, msg, err := c.ReadMessage()
		if err != nil {
			log.Println(err)
			break
		}

		var data Message
		err = json.Unmarshal(msg, &data)
		if err != nil {
			log.Println(err)
			break
		}

		user, err := middlewere.GetUser(data.Authorization)
		if err != nil {
			break
		}

		response := Response{
			Name:    user.Login,
			Message: data.Message,
		}

		responseJSON, err := json.Marshal(response)
		if err != nil {
			log.Println(err)
			break
		}

		id := uuid.New()
		currentTime := time.Now()
		timeStamp := currentTime.Format("02-01-2006 15:04:05")
		
		message := infrastructure.Message{
			ID:   id.String(),
			Text: response.Message,
			Date: timeStamp,
		}

		err = infrastructure.SendMessage(int64(user.Id), roomID, message)
		if err != nil {
			log.Println(err)
			break
		}

		rooms.RLock()
		for client := range rooms.clients {
			if client.roomID == roomID {
				if err := client.conn.WriteMessage(websocket.TextMessage, responseJSON); err != nil {
					log.Println(err)
				}
			}
		}
		rooms.RUnlock()
	}
}
