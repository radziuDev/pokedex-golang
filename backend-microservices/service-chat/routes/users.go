package routes

import (
	"chat/infrastructure"
	"github.com/gofiber/fiber/v2"
)

func GetUsers(c *fiber.Ctx) error {
	queries := c.Queries()
	search := queries["name"]

	users, err := infrastructure.GetUsersByMatch(search)
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	c.JSON(users)
	return c.SendStatus(fiber.StatusOK)
}
