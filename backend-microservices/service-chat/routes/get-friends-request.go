package routes

import (
	"chat/infrastructure"
	"chat/middlewere"

	"github.com/gofiber/fiber/v2"
)

func getFriendsRequests(c *fiber.Ctx) error {
	user := c.Locals("user").(middlewere.User)

	users, err := infrastructure.GetFriendsRequest(int64(user.Id))
	if err != nil {
		c.SendString(err.Error())
		return c.SendStatus(fiber.StatusBadRequest)
	}

	c.JSON(users)
	return c.SendStatus(fiber.StatusOK)
}
