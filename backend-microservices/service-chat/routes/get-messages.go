package routes

import (
	"chat/infrastructure"
	"net/url"

	"github.com/gofiber/fiber/v2"
)

func getMessagesForRoom(c *fiber.Ctx) error {	
	encodedRoomID := c.Params("roomID")
	roomID, err := url.QueryUnescape(encodedRoomID)
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	_, err = infrastructure.GetRoom(roomID)
	if err != nil {
		c.SendString(err.Error())
		return c.SendStatus(fiber.StatusBadRequest)
	}

	messages, err := infrastructure.GetMessagesFromRoom(roomID)
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	c.JSON(messages)
	return c.SendStatus(fiber.StatusOK)
}
