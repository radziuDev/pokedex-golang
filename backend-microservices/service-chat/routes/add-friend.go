package routes

import (
	"chat/infrastructure"
	"chat/middlewere"
	"fmt"

	"github.com/gofiber/fiber/v2"
)

type requestBodys struct {
	UserID int64 `json:"userId"`
}

func addFriend(c *fiber.Ctx) error {
	user := c.Locals("user").(middlewere.User)

	requestBody := new(requestBodys)

	if err := c.BodyParser(requestBody); err != nil {
		return c.SendStatus(fiber.StatusBadRequest)
	}

	err := infrastructure.DeleteFriendRequest(int64(user.Id), requestBody.UserID)
	if err != nil {
		c.SendString(err.Error())
		return c.SendStatus(fiber.StatusBadRequest)
	}

	user2, err := infrastructure.GetUser(requestBody.UserID)
	if err != nil {
		c.SendString(err.Error())
		return c.SendStatus(fiber.StatusBadRequest)
	}

	roomName := fmt.Sprintf("%v x %v", user.Login, user2.Login)
	err = infrastructure.CreateRoom(roomName)
	if err != nil {
		c.SendString(err.Error())
		return c.SendStatus(fiber.StatusBadRequest)
	}

	err =  infrastructure.JoinRoom(int64(user.Id), roomName)
	if err != nil {
		c.SendString(err.Error())
		return c.SendStatus(fiber.StatusBadRequest)
	}

	err =  infrastructure.JoinRoom(user2.ID, roomName)
	if err != nil {
		c.SendString(err.Error())
		return c.SendStatus(fiber.StatusBadRequest)
	}

	return c.SendStatus(fiber.StatusOK)
}

func addFriendRequest(c *fiber.Ctx) error {
	user := c.Locals("user").(middlewere.User)

	requestBody := new(requestBodys)

	if err := c.BodyParser(requestBody); err != nil {
		return c.SendStatus(fiber.StatusBadRequest)
	}

	err := infrastructure.CreateFriendRequest(int64(user.Id), requestBody.UserID)
	if err != nil {
		c.SendString(err.Error())
		return c.SendStatus(fiber.StatusBadRequest)
	}

	return c.SendStatus(fiber.StatusOK)
}
