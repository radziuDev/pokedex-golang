package routes

import (
	"chat/infrastructure"
	"chat/middlewere"

	"github.com/gofiber/fiber/v2"
)

func getRoomsForUser(c *fiber.Ctx) error {
	user := c.Locals("user").(middlewere.User)

	rooms, err := infrastructure.GetRoomsForUser(int64(user.Id))
	if err != nil {
		c.SendString(err.Error())
		return c.SendStatus(fiber.StatusBadRequest)
	}

	c.JSON(rooms)
	return c.SendStatus(fiber.StatusOK)
}

