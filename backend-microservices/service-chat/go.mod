module chat

go 1.20

require (
	github.com/gofiber/fiber/v2 v2.47.0
	github.com/gofiber/websocket/v2 v2.2.1
)

require (
	github.com/andybalholm/brotli v1.0.5 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fasthttp/websocket v1.5.3 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/jmcvetta/neoism v1.3.1 // indirect
	github.com/jmcvetta/randutil v0.0.0-20150817122601-2bb1b664bcff // indirect
	github.com/klauspost/compress v1.16.5 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/kr/text v0.1.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/neo4j/neo4j-go-driver/v5 v5.10.0 // indirect
	github.com/philhofer/fwd v1.1.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rabbitmq/amqp091-go v1.8.1 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/savsgio/dictpool v0.0.0-20221023140959-7bf2e61cea94 // indirect
	github.com/savsgio/gotils v0.0.0-20230208104028-c358bd845dee // indirect
	github.com/stretchr/testify v1.8.0 // indirect
	github.com/tinylib/msgp v1.1.8 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.47.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.9.0 // indirect
	gopkg.in/jmcvetta/napping.v3 v3.2.0 // indirect
	gopkg.in/jmcvetta/neoism.v1 v1.3.1 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
