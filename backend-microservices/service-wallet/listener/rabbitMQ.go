package listener

import (
	"encoding/json"
	"log"
	"sync"
	"wallet/config"
	"wallet/tasks"

	amqp "github.com/rabbitmq/amqp091-go"
)

type Message struct {
	Id int `json:"id"`
	Login string `json:"login"`
}

func InitRabbitMq(wg *sync.WaitGroup) {
	defer wg.Done()

	conn, err := amqp.Dial(config.Config.RabbitMqUrl)
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"",
		false,
		false,
		true,
		false,
		nil,
	)
	failOnError(err, "Failed to declare a queue")

	err = ch.QueueBind(
		q.Name,
		"",
		"CreateUser",
		false,
		nil,
	)
	failOnError(err, "Failed to bind a queue")

	msgs, err := ch.Consume(
		q.Name,
		"",
		false,
		false,
		false,
		false,
		nil,
	)

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			var account Message
			err := json.Unmarshal(d.Body, &account)
			if err == nil {
				tasks.CreateWallet(account.Id)
			}
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}
