package routes

import (
	"fmt"
	"log"
	"sync"
	"wallet/config"
	"wallet/middlewere"

	"github.com/gofiber/fiber/v2"
)

func InitAPI(wg *sync.WaitGroup) {
	defer wg.Done()

  app := fiber.New()
	app.Get("/wallet/balance", middlewere.AuthMiddlewere, getBalance)

	log.Fatalln(app.Listen(fmt.Sprintf(":%v", config.Config.Port)))
	log.Printf("User service runnig at port %v", config.Config.Port)
}
