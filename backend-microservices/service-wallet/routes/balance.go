package routes

import (
	"database/sql"
	"log"
	"wallet/db"
	"wallet/middlewere"

	"github.com/gofiber/fiber/v2"
)

type Balance struct {
  Coins int `json:"coins"`
}

func getBalance(c *fiber.Ctx) error {
  user := c.Locals("user").(middlewere.User)

	row := db.DB.QueryRow("SELECT coins FROM wallets WHERE user_id = $1", user.Id)

	var coins int
  err := row.Scan(&coins)
	if err != nil {
		if err == sql.ErrNoRows {
			return c.SendStatus(fiber.StatusUnauthorized)
		} else {
			log.Println(err)
			return c.SendStatus(fiber.StatusInternalServerError)
		}
	}
  
  balance := Balance {
    Coins: coins,
  }

  c.JSON(balance) 
	return c.SendStatus(fiber.StatusOK)
}
