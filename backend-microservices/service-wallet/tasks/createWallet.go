package tasks

import (
	"log"
	"wallet/db"
)

func CreateWallet(userId int) {
  var id int
	query := "INSERT INTO wallets (user_id, coins) VALUES ($1, $2) RETURNING id"
  row := db.DB.QueryRow(query, userId, 1000)
  row.Scan(&id)

  log.Printf("Created wallet %v for user %v", id, userId)
}
