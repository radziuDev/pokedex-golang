package main

import (
	"log"
	"sync"
	"wallet/config"
	"wallet/db"
	"wallet/listener"
	"wallet/routes"

	_ "github.com/lib/pq"
)

func main() {
	config.LoadConfigs()

  err := db.Init()
	if err != nil {
		log.Println(err)
		return
	}

  var wg sync.WaitGroup
  wg.Add(2)
  go routes.InitAPI(&wg)
	go listener.InitRabbitMq(&wg)
  wg.Wait()
}
