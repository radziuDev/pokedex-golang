package db

import (
	"database/sql"
	"fmt"
	"wallet/config"
)

var DB *sql.DB

func Init() error {
	connStr := fmt.Sprintf("postgres://postgres:postgres@service-wallet-db:%v/wallet?sslmode=disable", config.Config.PostgresPort)

	db, err := sql.Open("postgres", connStr)
	DB = db

  return err
}
