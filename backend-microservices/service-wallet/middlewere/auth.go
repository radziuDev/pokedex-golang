package middlewere

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"wallet/config"

	"github.com/gofiber/fiber/v2"
)

type User struct {
	Id    int    `json:"Id"`
	Login string `json:"Login"`
}

func AuthMiddlewere(c *fiber.Ctx) error {
	headers := c.GetReqHeaders()
	auth := headers["Authorization"]
	if auth == "" {
		return fiber.NewError(fiber.StatusUnauthorized, "Unauthorized")
	}

	req, err := http.NewRequest("GET", fmt.Sprintf("%v/user/me", config.Config.UserServiceUrl), nil)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, "Internal error")
	}

	req.Header.Add("Authorization", auth)
	client := http.DefaultClient
	resp, err := client.Do(req)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, "Internal error")
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusUnauthorized {
		return fiber.NewError(fiber.StatusUnauthorized, "Unauthorized")
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, "Internal error")
	}

	var user User
	err = json.Unmarshal(body, &user)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, "Internal error")
	}

	c.Locals("user", user)
	return c.Next()
}
