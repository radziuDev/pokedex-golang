package config

import (
	"log"
	"os"
	"reflect"
	"strconv"
)

func setConfig(confVariable string, envVariable string, isInt bool, required bool, defaultValue string) {
	env := os.Getenv(envVariable)

	if isInt {
		envInt, envErr := strconv.Atoi(env)
		if envErr != nil {
			log.Printf("%v port unconfigured!", confVariable)
			os.Exit(1)
		}

		if required && env == "" {
			log.Printf("%v port unconfigured!", confVariable)
			os.Exit(1)
		}

		if env == "" {
			convDefault, convErr := strconv.Atoi(defaultValue)
			if convErr != nil {
				log.Printf("%v: error at converting!", confVariable)
			}

			setField(&Config, confVariable, convDefault)
		} else {
			setField(&Config, confVariable, envInt)
		}
	} else {
		if required && env == "" {
			log.Printf("%v port unconfigured!", confVariable)
			os.Exit(1)
		}

		if env == "" {
			setField(&Config, confVariable, defaultValue)
		} else {
			setField(&Config, confVariable, env)
		}
	}
}

func setField(obj interface{}, fieldName string, value interface{}) {
	structValue := reflect.ValueOf(obj).Elem()
	structFieldValue := structValue.FieldByName(fieldName)

	if !structFieldValue.IsValid() {
		log.Printf("Invalid field: %s", fieldName)
		return
	}

	if !structFieldValue.CanSet() {
		log.Printf("Cannot set field: %s", fieldName)
		return
	}

	fieldValue := reflect.ValueOf(value)
	if structFieldValue.Type() != fieldValue.Type() {
		log.Printf("Invalid type for field: %s", fieldName)
		return
	}

	structFieldValue.Set(fieldValue)
}

