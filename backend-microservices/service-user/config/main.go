package config

type ConfigStruct struct {
	Port              int
	PostgresPort      int
  RabbitMqUrl      string
}

var Config ConfigStruct

func LoadConfigs() {
	setConfig("Port", "PORT", true, false, "3000")
	setConfig("RabbitMqUrl", "RABBITMQ_URL", false, true, "")
	setConfig("PostgresPort", "POSTGRES_PORT", true, true, "")
}
