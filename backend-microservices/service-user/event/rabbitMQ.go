package event

import (
	"log"
	"sync"
	"user/config"

	amqp "github.com/rabbitmq/amqp091-go"
)

var Conn *amqp.Connection
var Channel *amqp.Channel

func InitRabbitMq(wg *sync.WaitGroup) {
	defer wg.Done()

	conn, err := amqp.Dial(config.Config.RabbitMqUrl)
	Conn = conn
	if err != nil {
		log.Panic(err)
	}

	Channel, err = conn.Channel()

	if err != nil {
		log.Panic(err)
	}

	err = Channel.ExchangeDeclare(
		"CreateUser",
		"fanout",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Panic(err)
	}
}

func Emit(exchange string, body []byte) error {
	var ch *amqp.Channel
	if exchange == "CreateUser" {
		ch = Channel
	}

	err := ch.Publish(
		exchange,
		"",
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        body,
		})

	if err != nil {
		return err
	}

	return nil
}
