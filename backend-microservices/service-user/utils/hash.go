package utils

import (
	"golang.org/x/crypto/bcrypt"
)

func HashString(toHash string) (error, string) {
	hashed, err := bcrypt.GenerateFromPassword([]byte(toHash), bcrypt.DefaultCost)

	if err != nil {
		return err, ""
	}

	return nil, string(hashed)
}

func CompareSting(hashed string, toCompare string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashed), []byte(toCompare))
	if err != nil {
		return false
	}
	return true
}
