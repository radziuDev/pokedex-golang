package main

import (
	"log"
	"sync"
	"user/config"
	"user/db"
	"user/event"
	"user/routes"

	_ "github.com/lib/pq"
)

func main() {
  config.LoadConfigs()

	err := db.Init()
	if err != nil {
		log.Println(err)
		return
	}
  
  var wg sync.WaitGroup
  wg.Add(2)
	go event.InitRabbitMq(&wg)
	go routes.Init(&wg)
  wg.Wait()
	
	defer event.Conn.Close()
	defer event.Channel.Close()
}

