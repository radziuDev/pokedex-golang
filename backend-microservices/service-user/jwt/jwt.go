package jwt

import (
	"crypto/rand"
	"encoding/base64"
	"errors"
	"strings"
	"time"
	"user/db"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
)

var jwtKey = []byte("secrey_key")

type Claims struct {
	UserId int
	jwt.StandardClaims
}

func GenerateJWT(userId int) (string, error) {
	expirationTime := time.Now().Add(time.Minute * 15)
	claims := &Claims{
		UserId: userId,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)

	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func VerifyJWT(c *fiber.Ctx) (int, error) {
	headers := c.GetReqHeaders()
	authBearer := headers["Authorization"]
	jwtToken := strings.TrimPrefix(authBearer, "Bearer ")

	claims := &Claims{}

	tkn, err := jwt.ParseWithClaims(jwtToken, claims,
		func(t *jwt.Token) (interface{}, error) {
			return jwtKey, nil
		})

	if err != nil || !tkn.Valid {
		return 0, fiber.ErrUnauthorized
	}

	return claims.UserId, nil
}

func GenerateRefreshToken(length int) (string, error) {
	tokenBytes := make([]byte, length)
	_, err := rand.Read(tokenBytes)
	if err != nil {
		return "", err
	}

  encodedToken := base64.RawURLEncoding.EncodeToString(tokenBytes)
  return encodedToken[:length], nil
}

func CreateTokenPair(id int) (string, string, error) {
	jwtToken, jwtErr := GenerateJWT(id)
	refreshToken, refreshErr := GenerateRefreshToken(128)

	if jwtErr != nil || refreshErr != nil {
    return "", "", errors.New("Can't generate tokens")
	}

	insertStmt, err := db.DB.Prepare("INSERT INTO refreshTokens (userId, token, created_at, expired_at) VALUES ($1, $2, $3, $4)")
	if err != nil {
		return "", "", err
	}
	defer insertStmt.Close()

	expirationDate := time.Now().Add(14 * 24 * time.Hour)

	_, err = insertStmt.Exec(id, refreshToken, time.Now(), expirationDate)
	if err != nil {
		return "", "", err
	}

	return jwtToken, refreshToken, nil
}

func DeleteRefreshTokenById(id int) (error) {
	deleteStmt, err := db.DB.Prepare("DELETE FROM refreshTokens WHERE userId = $1")
	if err != nil {
		return err
	}
	defer deleteStmt.Close()

	_, err = deleteStmt.Exec(id)
	if err != nil {
		return err
	}

  return nil
}

func DeleteRefreshTokenByToken(token string) (error) {
	deleteStmt, err := db.DB.Prepare("DELETE FROM refreshTokens WHERE token = $1")
	if err != nil {
		return err
	}
	defer deleteStmt.Close()

	_, err = deleteStmt.Exec(token)
	if err != nil {
		return err
	}

  return nil
}
