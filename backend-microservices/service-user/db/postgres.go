package db

import (
	"database/sql"
	"fmt"
	"user/config"
)

var DB *sql.DB

func Init() error {
	connStr := fmt.Sprintf("postgres://postgres:postgres@service-user-db:%v/user?sslmode=disable", config.Config.PostgresPort)

	db, err := sql.Open("postgres", connStr)
	DB = db

	return err
}
