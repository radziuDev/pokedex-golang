package logout

import (
	"database/sql"
	"log"
	"user/db"
	"user/jwt"

	"github.com/gofiber/fiber/v2"
)


func HandleLogout(c *fiber.Ctx) error {
  headers := c.GetReqHeaders()
	refreshTk := headers["Refreshtoken"]

	row := db.DB.QueryRow("SELECT userId FROM refreshTokens WHERE token = $1", refreshTk)

	var userId int
	err := row.Scan(&userId)

	if err != nil {
		if err == sql.ErrNoRows {
			c.SendString("account not found")
			return c.SendStatus(fiber.StatusUnauthorized)
		} else {
			log.Println(err)
			return c.SendStatus(fiber.StatusInternalServerError)
		}
	}

  err = jwt.DeleteRefreshTokenByToken(refreshTk)
  if err != nil {
		log.Println(err)
		return c.SendStatus(fiber.StatusInternalServerError)
  }

	return c.SendStatus(fiber.StatusOK)
}
