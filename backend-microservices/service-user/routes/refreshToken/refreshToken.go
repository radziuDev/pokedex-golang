package refreshtoken

import (
	"database/sql"
	"log"
	"time"
	"user/db"
	"user/jwt"

	"github.com/gofiber/fiber/v2"
)


func HandleRefresh(c *fiber.Ctx) error {
  headers := c.GetReqHeaders()
	refreshTk := headers["Refreshtoken"]
	row := db.DB.QueryRow("SELECT userId FROM refreshTokens WHERE token = $1", refreshTk)

	var userId int
	err := row.Scan(&userId)

	if err != nil {
		if err == sql.ErrNoRows {
			c.SendString("You provide wrong login or password!")
			return c.SendStatus(fiber.StatusUnauthorized)
		} else {
			log.Println(err)
			return c.SendStatus(fiber.StatusInternalServerError)
		}
	}

  err = jwt.DeleteRefreshTokenByToken(refreshTk)
  if err != nil {
		log.Println(err)
		return c.SendStatus(fiber.StatusInternalServerError)
  }

	jwtToken, refreshToken, err := jwt.CreateTokenPair(userId)
	if err != nil {
		log.Println(err)
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	c.Cookie(&fiber.Cookie{
		Name:     "jwtToken",
		Value:    jwtToken,
		Expires:  time.Now().Add(15 * time.Minute),
		HTTPOnly: true,
		SameSite: "lax",
	})

	c.Cookie(&fiber.Cookie{
		Name:     "refreshToken",
		Value:    refreshToken,
		Expires:  time.Now().Add(14 * 24 * time.Hour),
		HTTPOnly: true,
		SameSite: "lax",
	})

	return c.SendStatus(fiber.StatusOK)
}

