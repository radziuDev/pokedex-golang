package register

import (
	"encoding/json"
	"log"
	"user/db"
	"user/event"
	"user/utils"

	"github.com/gofiber/fiber/v2"
)

type registerForm struct {
	Login    string
	Password string
}

type Message struct {
	Id    int    `json:"id"`
	Login string `json:"login"`
}

func HandleRegister(c *fiber.Ctx) error {
	form := registerForm{}
	formErr := c.BodyParser(&form)

	if formErr != nil {
		log.Println(formErr)
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	row := db.DB.QueryRow("SELECT EXISTS(SELECT 1 FROM users WHERE login = $1)", form.Login)
	var exists bool
	err := row.Scan(&exists)
	if err != nil {
		log.Println(err)
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	if exists {
		c.SendString("User already exists!")
		return c.SendStatus(fiber.ErrConflict.Code)
	}

	err, password := utils.HashString(form.Password)

	var id int
	var login string
	query := "INSERT INTO users (login, password) VALUES ($1, $2) RETURNING id, login"
	row = db.DB.QueryRow(query, form.Login, password)
	err = row.Scan(&id, &login)
	if err != nil {
		log.Println(err)
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	jsonMessage := Message{
		Id:    int(id),
		Login: login,
	}

	message, err := json.Marshal(jsonMessage)
	if err != nil {
		log.Println(err)
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	err = event.Emit("CreateUser", message)
	if err != nil {
		log.Println(err)
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	c.SendString("User created!")
	return c.SendStatus(fiber.StatusCreated)
}
