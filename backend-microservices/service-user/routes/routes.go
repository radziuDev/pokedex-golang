package routes

import (
	"fmt"
	"log"
	"sync"
	"user/config"
	"user/routes/login"
	"user/routes/logout"
	"user/routes/me"
	"user/routes/refreshToken"
	"user/routes/register"

	"github.com/gofiber/fiber/v2"
)

func Init(wg *sync.WaitGroup) {
	defer wg.Done()

	app := fiber.New()

	app.Post("/user/login", login.HandleLogin)
	app.Get("/user/logout", logout.HandleLogout)
	app.Post("/user/register", register.HandleRegister)
	app.Post("/user/refresh", refreshtoken.HandleRefresh)
	app.Get("/user/me", me.HandleMe)

	log.Fatalln(app.Listen(fmt.Sprintf(":%v", config.Config.Port)))
	log.Printf("User service runnig at port %v", config.Config.Port)
}
