package me

import (
	"database/sql"
	"log"
	"user/db"
	"user/jwt"

	"github.com/gofiber/fiber/v2"
)

type Me struct {
	Id  int `json:"id"`
	Login string `json:"login"`
}

func HandleMe(c *fiber.Ctx) error {
  userId, err := jwt.VerifyJWT(c)

	if err != nil {
		return c.SendStatus(fiber.StatusUnauthorized)
	}

	row := db.DB.QueryRow("SELECT id, login FROM users WHERE id = $1", userId)

	var id int
	var login string
	err = row.Scan(&id, &login)

	if err != nil {
		if err == sql.ErrNoRows {
			return c.SendStatus(fiber.StatusUnauthorized)
		} else {
			log.Println(err)
			return c.SendStatus(fiber.StatusInternalServerError)
		}
	}
  
  me := Me {
    Id: id,
    Login: login,
  }

  c.JSON(me) 
	return c.SendStatus(fiber.StatusOK)
}
