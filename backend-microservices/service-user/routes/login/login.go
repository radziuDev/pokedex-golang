package login

import (
	"database/sql"
	"fmt"
	"log"
	"time"
	"user/db"
	"user/jwt"
	"user/utils"

	"github.com/gofiber/fiber/v2"
)

type loginForm struct {
	Login    string
	Password string
}

func HandleLogin(c *fiber.Ctx) error {
	form := loginForm{}
	formErr := c.BodyParser(&form)

	if formErr != nil {
		log.Println(formErr)
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	row := db.DB.QueryRow("SELECT id, password FROM users WHERE login = $1", form.Login)

	var id int
	var password string

	err := row.Scan(&id, &password)

	if err != nil {
		if err == sql.ErrNoRows {
			c.SendString("You provide wrong login or password!")
			return c.SendStatus(fiber.StatusUnauthorized)
		} else {
			log.Println(err)
			return c.SendStatus(fiber.StatusInternalServerError)
		}
	}

	isValidPassword := utils.CompareSting(password, form.Password)
	if !isValidPassword {
		c.SendString("You provide wrong login or password!")
		return c.SendStatus(fiber.StatusUnauthorized)
	}

	jwtToken, refreshToken, err := jwt.CreateTokenPair(id)
	if err != nil {
		fmt.Println(err)
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	c.Cookie(&fiber.Cookie{
		Name:     "jwtToken",
		Value:    jwtToken,
		Expires:  time.Now().Add(15 * time.Minute),
		HTTPOnly: true,
		SameSite: "lax",
	})

	c.Cookie(&fiber.Cookie{
		Name:     "refreshToken",
		Value:    refreshToken,
		Expires:  time.Now().Add(14 * 24 * time.Hour),
		HTTPOnly: true,
		SameSite: "lax",
	})

	return c.SendStatus(fiber.StatusOK)
}
