package main

import (
	"fmt"
	"gateway/config"
	"gateway/shared"
	"log"
	"net/http"
)

func main() {
	config.LoadConfigs()
	shared.InitServices()

	http.HandleFunc("/", shared.Redirect)

	log.Printf("Api Gateway runnig at port %v", config.Config.Port)
	log.Fatalln(http.ListenAndServe(fmt.Sprintf(":%v", config.Config.Port), nil))
}
