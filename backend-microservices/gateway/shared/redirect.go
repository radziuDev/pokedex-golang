package shared

import (
	"fmt"
	"gateway/config"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
)

func Redirect(w http.ResponseWriter, r *http.Request) {
	if strings.ToLower(r.Header.Get("Upgrade")) == "websocket" {
		r.Header.Del("Origin")
	} else {
		w.Header().Set("Access-Control-Allow-Origin", fmt.Sprintf("http://localhost:%v", config.Config.FrontendPort))
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
	}

	if r.Method == http.MethodOptions {
		w.WriteHeader(http.StatusOK)
		return
	}

	parts := strings.Split(r.URL.Path, "/")
	if len(parts) <= 0 {
		http.Error(w, "Service not found", http.StatusNotFound)
		return
	}

	serviceName := parts[1]
	service := getService(serviceName)
	if service == nil {
		http.Error(w, "Service not found", http.StatusNotFound)
		return
	}

	endpoint, err := url.Parse(service.Endpoint)
	if err != nil {
		log.Fatalf("Error parsing endpoint URL for %s: %v", service.Name, err)
	}

	proxy := httputil.NewSingleHostReverseProxy(endpoint)
	proxy.ServeHTTP(w, r)
}
