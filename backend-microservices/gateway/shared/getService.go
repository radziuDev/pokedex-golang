package shared

import (
	"fmt"
	"gateway/config"
)

type Service struct {
	Name     string
	Endpoint string
}

var services []Service

func InitServices() {
	services = []Service{
		{
			Name:     "user",
			Endpoint: fmt.Sprintf("http://service-user:%v", config.Config.UserServicePort),
		},
		{
			Name:     "pokedex",
			Endpoint: fmt.Sprintf("http://service-pokedex:%v", config.Config.PokedexServicePort),
		},
		{
			Name:     "wallet",
			Endpoint: fmt.Sprintf("http://service-wallet:%v", config.Config.WalletServicePort),
		},
		{
			Name:     "chat",
			Endpoint: fmt.Sprintf("http://service-chat:%v", config.Config.ChatServicePort),
		},
	}
}

func getService(path string) *Service {
	for _, service := range services {
		if path == service.Name {
			return &service
		}
	}
	return nil
}
