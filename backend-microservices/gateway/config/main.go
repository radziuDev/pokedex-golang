package config

type ConfigStruct struct {
	Port               int
	UserServicePort    int
	WalletServicePort  int
	PokedexServicePort int
	ChatServicePort    int
	FrontendPort       int
}

var Config ConfigStruct

func LoadConfigs() {
	setConfig("Port", "PORT", true, false, "3000")
	setConfig("UserServicePort", "USER_SERVICE_PORT", true, true, "")
	setConfig("WalletServicePort", "WALLET_SERVICE_PORT", true, true, "")
	setConfig("PokedexServicePort", "POKEDEX_SERVICE_PORT", true, true, "")
	setConfig("ChatServicePort", "CHAT_SERVICE_PORT", true, true, "")
	setConfig("FrontendPort", "FRONTEND_PORT", true, true, "")
}
