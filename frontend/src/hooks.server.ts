import { redirect, type ResolveOptions } from "@sveltejs/kit";
import type { RequestEvent } from "./routes/(app)/dashboard/$types";
import API from "$lib/api";

let access = false;
const clearCookies = async (event: RequestEvent, refreshToken: string | undefined) => {
  event.cookies.delete("jwtToken");
  event.cookies.delete("refreshToken");
  access = false;

  try {
    await API.get('/user/logout', {}, {
      refreshToken: refreshToken,
    });
  } catch { /**/ }
}

/** @type {import('@sveltejs/kit').Handle} */
export async function handle({ event, resolve }: { event: RequestEvent, resolve: ResolveOptions }) {
  access = false;

  if (!event.route.id?.startsWith("/(auth)/logout")) {
    const refreshToken = event.cookies.get("refreshToken");
    const jwtToken = event.cookies.get("jwtToken");

    if (refreshToken && refreshToken !== "" && jwtToken && jwtToken !== "") {
      try {
        const { status, headers } = await API.post('/user/refresh', {}, {
          refreshToken: refreshToken,
        });

        access = status === 200;
        if (headers["set-cookie"]) {
          headers["set-cookie"].forEach((cookie: string) => {
            const cookieParts = cookie.split(";");

            const [name, value] = cookieParts[0].split("=");
            const cookieOptions: Record<string, string> = {};

            for (let i = 1; i < cookieParts.length; i++) {
              const [key, val] = cookieParts[i].split("=");
              cookieOptions[key?.trim().toLowerCase()] = val?.trim();
            }

            const options = {
              path: cookieOptions.path || "/",
              expires: cookieOptions.expires ? new Date(cookieOptions.expires) : undefined,
              domain: cookieOptions.domain || undefined,
              secure: cookieOptions.secure ? true : false,
              httponly: cookieOptions.httponly ? true : false,
              samesite: cookieOptions.samesite || undefined,
            };

            event.cookies.set(name.trim(), value.trim(), options);
          });

          try {
            const { data } = await API.get('/user/me', {}, {
              Authorization: `Bearer ${jwtToken}`
            });
            (event.locals as Record<string, unknown>).user = data;
            const { data: balance } = await API.get('/wallet/balance', {}, {
              Authorization: `Bearer ${jwtToken}`,
            });
            (event.locals as Record<string, unknown>).coins = (balance as { coins: number }).coins;
            (event.locals as Record<string, unknown>).jwtToken = `Bearer ${jwtToken}`;
          } catch (error) {
            console.log(error);
            await clearCookies(event, refreshToken);
          }
        }
      } catch (error: unknown) {
        console.log(error)
        await clearCookies(event, refreshToken);
      }
    } else {
      await clearCookies(event, refreshToken);
    }
  }

  (event.locals as Record<string, unknown>).authedUser = access ? true : undefined;

  if (!access && event.route.id?.startsWith("/(app)")) {
    throw redirect(302, "/login")
  }

  if (access && event.route.id?.startsWith("/(auth)")) {
    if (!event.route.id?.startsWith("/(auth)/logout")) {
      throw redirect(302, "/")
    }
  }

  const response = await resolve(event);
  return response;
}
