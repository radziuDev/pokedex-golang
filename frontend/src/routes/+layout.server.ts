export async function load({ locals }: { locals: Record<string, unknown> }) {
  let authedUser = undefined;
  let user = undefined;
  let coins = undefined;
  let jwtToken = undefined;

  if (locals.jwtToken) jwtToken = locals.jwtToken;
  if (locals.authedUser) authedUser = locals.authedUser;
  if (locals.user) user = locals.user;
  if (locals.coins) coins = locals.coins;

  return { authedUser, user, coins, jwtToken };
}
