import API from '$lib/api';
import { fail, redirect, type Actions } from '@sveltejs/kit'
import type { AxiosError } from 'axios';

interface Locals {
  authedUser: boolean;
}

export const load = async ({ locals }: { locals: Locals }) => {
  if (locals.authedUser) {
    throw redirect(302, '/')
  }
}

export const actions: Actions = {
  async default({ request, cookies, locals }) {
    const data = await request.formData();
    const login = data.get('login');
    const password = data.get('password');

    try {
      const { status, headers, data } = await API.post('/user/login', { login, password});

      if (status === 200 && headers["set-cookie"]) {
        headers["set-cookie"].forEach((cookie: string) => {
          const cookieParts = cookie.split(";");

          const [name, value] = cookieParts[0].split("=");
          const cookieOptions: Record<string, string> = {};

          for (let i = 1; i < cookieParts.length; i++) {
            const [key, val] = cookieParts[i].split("=");
            cookieOptions[key?.trim().toLowerCase()] = val?.trim();
          }

          const options = {
            path: cookieOptions.path || "/",
            expires: cookieOptions.expires ? new Date(cookieOptions.expires) : undefined,
            domain: cookieOptions.domain || undefined,
            secure: cookieOptions.secure ? true : false,
            httponly: cookieOptions.httponly ? true : false,
            samesite: cookieOptions.samesite || undefined,
          };

          cookies.set(name.trim(), value.trim(), options);
        });

        if (locals) ((locals) as Record<string, boolean>).authedUser = true;
        redirect(302, '/dashboard')
      } else {
        return fail(500, { error: data });
      }
    } catch (error: unknown) {
      const axiosError = error as AxiosError;
      if ((axiosError.toJSON() as AxiosError).status === 401) {
        return fail(401, { error: "Provided wrong password or login!" })
      }

      return fail(500, { error: error })
    }
  },
}
