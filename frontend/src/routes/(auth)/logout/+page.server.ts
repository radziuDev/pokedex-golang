import API from '$lib/api';
import { redirect, type Actions } from '@sveltejs/kit'

interface Locals {
  authedUser: boolean;
}

export const load = async ({ locals }: { locals: Locals }) => {
  if (!locals.authedUser) {
    throw redirect(302, '/');
  }
}

export const actions: Actions = {
  async default({ cookies }) {
    try {
      const refreshToken = cookies.get('refreshToken');
      await API.get('/user/logout', {}, {
        refreshToken: refreshToken,
      });
    } catch (error) { /**/ }

    throw redirect(302, '/')
  },
}
