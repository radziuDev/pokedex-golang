import API from '$lib/api';
import { fail, redirect, type Actions } from '@sveltejs/kit'
import type { AxiosError } from 'axios';

interface Locals {
  authedUser: boolean;
  created: boolean;
}

export const load = async ({ locals }: { locals: Locals }) => {
  if (locals.created) {
    locals.created = false;
    throw redirect(302, '/login');
  }

  if (locals.authedUser) {
    throw redirect(302, '/');
  }
}

export const actions: Actions = {
  async default({ request, locals }) {
    const data = await request.formData();
    const login = data.get('login');
    const password = data.get('password');
    const retypePassword = data.get('retypePassword');

    if ((login as string).length < 5) {
      return fail(400, { error: "Login is to short (min 5 chars)" });
    }

    if ((password as string).length < 5) {
      return fail(400, { error: "Password is to short (min 5 chars)" });
    }

    if (password !== retypePassword) {
      return fail(400, { error: "Paswords are not the same!" });
    }

    try {
      const { status, data } = await API.post("/user/register", { login, password });

      if (status !== 201) {
        return fail(400, { error: data });
      } else {
        (locals as Locals).created = true;
        throw redirect(302, "/login");
      }
    } catch (error: unknown) {
      const axiosError = error as AxiosError;
      if ((axiosError as AxiosError).response?.status === 409) {
        return fail(409, { error: "Provided user already exist!" });
      }

      return fail(500, { error: axiosError.message });
    }
  },
}
