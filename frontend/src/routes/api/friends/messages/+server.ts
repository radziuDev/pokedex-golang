import API from "$lib/api";
import type { RequestHandler } from "@sveltejs/kit";

export const GET: RequestHandler = async ({ request }) => {
	const url = new URL(request.url);
	const queryParams = new URLSearchParams(url.search);
	const roomId = queryParams.get("v");

	let data = {};
	try {
		const { data: value } = await API.get(`chat/messages/${roomId}`, {}, {
			"authorization": request.headers.get("authorization")
		});
		data = value as Record<string, any>;
	} catch (error) {
		console.log(error);
		return new Response(JSON.stringify({ success: false, error, data }));
	}

	return new Response(JSON.stringify({ success: true, error: null, data }));
};
