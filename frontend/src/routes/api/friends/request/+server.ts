import API from "$lib/api";
import type { RequestHandler } from "@sveltejs/kit";

export const GET: RequestHandler = async ({ request }) => {
	let data = {};
	try {
		const { data: value } = await API.get("chat/friends/request", {}, {
			"authorization": request.headers.get("authorization")
		});
		data = value as Record<string, any>;
	} catch (error) {
		console.log(error);
		return new Response(JSON.stringify({ success: false, error, data }));
	}

	return new Response(JSON.stringify({ success: true, error: null, data }));
};

export const POST: RequestHandler = async ({ request }) => {
	let data = {};
	const body = await request.json();
	try {
		const { data: value } = await API.post(`chat/friends/request`, body, {
			"authorization": request.headers.get("authorization")
		});
		data = value as Record<string, any>;
	} catch (error) {
		console.log(error);
		return new Response(JSON.stringify({ success: false, error, data }));
	}

	return new Response(JSON.stringify({ success: true, error: null, data }));
};
