import API from "$lib/api";
import type { RequestHandler } from "./$types";

export const GET: RequestHandler = async ({ request }) => {
	const url = new URL(request.url);
	const queryParams = new URLSearchParams(url.search);
	const v = queryParams.get("v");

	let data = {};
	try {
		const { data: value } = await API.get(`pokedex/pokemons?name=${v}`, {}, {
			"authorization": request.headers.get("authorization")
		});
		data = value as Record<string, any>;
	} catch (error) {
		console.log(error);
		return new Response(JSON.stringify({ success: false, error, data }));
	}

	return new Response(JSON.stringify({ success: true, error: null, data }));
};
