export async function load({ locals }: { locals: Record<string, unknown> }) {
  let user = undefined
  if (locals.user) user = locals.user;

  return { user }
}
