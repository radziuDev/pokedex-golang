import axios, { type AxiosInstance, type AxiosRequestConfig, type AxiosResponse } from "axios";
import dotenv from "dotenv";

dotenv.config();

const axiosAPI: AxiosInstance = axios.create({
  withCredentials: true,
  baseURL: process.env.GATEWAY_URL,
});

const apiRequest = <T>(method: string, url: string, request?: Record<string, any>, headers?: Record<string, any>): Promise<AxiosResponse<T>> => {
  const config: AxiosRequestConfig = {
    method,
    url,
    data: request,
    headers,
  };

  return axiosAPI
    .request<T>(config)
    .then((res: AxiosResponse<T>) => {
      return Promise.resolve(res);
    })
    .catch((err: Error) => {
      return Promise.reject(err);
    });
};

const get = <T>(url: string, request?: Record<string, any>, headers: Record<string, any> = {}): Promise<AxiosResponse<T>> =>
  apiRequest<T>("get", url, request, headers);

const deleteRequest = <T>(url: string, request?: Record<string, any>, headers: Record<string, any> = {}): Promise<AxiosResponse<T>> =>
  apiRequest<T>("delete", url, request, headers);

const post = <T>(url: string, request?: Record<string, any>, headers: Record<string, any> = {}): Promise<AxiosResponse<T>> =>
  apiRequest<T>("post", url, request, headers);

const put = <T>(url: string, request?: Record<string, any>, headers: Record<string, any> = {}): Promise<AxiosResponse<T>> =>
  apiRequest<T>("put", url, request, headers);

const patch = <T>(url: string, request?: Record<string, any>, headers: Record<string, any> = {}): Promise<AxiosResponse<T>> =>
  apiRequest<T>("patch", url, request, headers);

const API = {
  get,
  delete: deleteRequest,
  post,
  put,
  patch,
};

export default API;

